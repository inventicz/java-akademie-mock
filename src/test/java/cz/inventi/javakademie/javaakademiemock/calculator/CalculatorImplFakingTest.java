package cz.inventi.javakademie.javaakademiemock.calculator;

import org.junit.Test;

/**
 *
 * @author Tomas Poledny <tomas.poledny at inventi.cz>
 */
public class CalculatorImplFakingTest {

	@Test
	public void addTestWithFaking() {
		int x = 8;
		int y = 4;
		int expected = x - y;
		int notExpected = x + y;

		// Pomoci fakingu (MockUp<T>) zamockujte metodu CalculatorImpl#add tak, aby odcitala

		CalculatorImpl calculator = new CalculatorImpl();

		int result = calculator.add(x, y);
		// pridejte overeni jak kladne pro expected tak i zaporne pro notExpected
	}

}
