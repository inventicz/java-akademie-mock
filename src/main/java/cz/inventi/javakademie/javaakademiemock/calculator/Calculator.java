package cz.inventi.javakademie.javaakademiemock.calculator;

/**
 *
 * @author Tomas Poledny <tomas.poledny at inventi.cz>
 */
public interface Calculator {

	int subtract(int a, int b);
	
	int add(int a, int b);
}
