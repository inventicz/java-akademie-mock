package cz.inventi.javakademie.javaakademiemock.calculator;

/**
 *
 * @author Tomas Poledny <tomas.poledny at inventi.cz>
 */
public interface Operation {

	int operation(int a, int b);
}
