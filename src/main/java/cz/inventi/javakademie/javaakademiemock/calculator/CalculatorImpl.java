package cz.inventi.javakademie.javaakademiemock.calculator;

import javax.inject.Inject;
import javax.inject.Named;

/**
 *
 * @author Tomas Poledny <tomas.poledny at inventi.cz>
 */
@Named
public class CalculatorImpl implements Calculator {
	
	@Inject
	private AddOperation addOperation;
	
	@Inject
	private SubtractOperation subtractOperation;
	

	@Override
	public int subtract(int a, int b) {
		return subtractOperation.operation(a, b);
	}

	@Override
	public int add(int a, int b) {
		return addOperation.operation(a, b);
	}
	
	
}
